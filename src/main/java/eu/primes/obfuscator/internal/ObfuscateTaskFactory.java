
package eu.primes.obfuscator.internal;

import org.cytoscape.application.CyApplicationManager;
import org.cytoscape.model.CyNetworkTableManager;
import org.cytoscape.view.model.CyNetworkViewManager;
import org.cytoscape.work.AbstractTaskFactory;
import org.cytoscape.work.TaskIterator;

/**
 * Task factory that creates new ObfuscateTask.
 * 
 * @author Ivan Hendy Goenawan
 */
public class ObfuscateTaskFactory extends AbstractTaskFactory {
	private CyApplicationManager appMgr;
	private CyNetworkViewManager netViewMgr;
	private CyNetworkTableManager netTableMgr;
	
	public ObfuscateTaskFactory(CyApplicationManager appMgr, CyNetworkViewManager netViewMgr, CyNetworkTableManager netTableMgr){
		this.appMgr = appMgr;
		this.netViewMgr = netViewMgr;
		this.netTableMgr = netTableMgr;
	}
	
	@Override
	public TaskIterator createTaskIterator() {
		return new TaskIterator(new ObfuscateTask(appMgr, netViewMgr, netTableMgr));
	}

	@Override
	public boolean isReady() {
		return super.isReady() && (appMgr.getSelectedNetworks().size() > 0);
	}
	
	
}
