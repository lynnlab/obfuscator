package eu.primes.obfuscator.internal;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Random;

import org.cytoscape.application.CyApplicationManager;
import org.cytoscape.model.CyColumn;
import org.cytoscape.model.CyEdge;
import org.cytoscape.model.CyEdge.Type;
import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyNetworkTableManager;
import org.cytoscape.model.CyNode;
import org.cytoscape.model.CyRow;
import org.cytoscape.model.CyTable;
import org.cytoscape.view.model.CyNetworkView;
import org.cytoscape.view.model.CyNetworkViewManager;
import org.cytoscape.view.model.View;
import org.cytoscape.view.presentation.property.BasicVisualLexicon;
import org.cytoscape.work.AbstractTask;
import org.cytoscape.work.TaskMonitor;
import org.cytoscape.work.Tunable;

/**
 * This task replaces names of nodes in a network with random names. Names of edges are also cleared.
 * To prevent names of nodes from getting left behind in the XGMML file when network is exported, 
 * bypass values for node and edge labels are deleted. There's also the option to obfuscate network names.
 * 
 * When more than one networks have been selected, nodes with the same names in different networks
 * will still be given identical new names, so as to maintain correspondence among nodes in different
 * networks.
 * 
 * @author Ivan Hendy Goenawan
 */
public class ObfuscateTask extends AbstractTask {
	private CyApplicationManager appMgr;
	private CyNetworkViewManager netViewMgr;
	private CyNetworkTableManager netTableMgr;
	private Random random;
	private HashMap<String, String> originalToNewNameMap;
	private HashSet<String> usedNames;
	
	
	private static final HashSet<String> defaultTableNames = new HashSet<String>(Arrays.asList
			(CyNetwork.DEFAULT_ATTRS, CyNetwork.HIDDEN_ATTRS, CyNetwork.LOCAL_ATTRS));
	
	private static final String CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	private static final int NAME_LENGTH = 6;   //308,915,776 unique names, should be enough
	
	
	
	@Tunable(description="Obfuscate network names?")
	public boolean obfuscateNetworkNames;
	
	@Tunable(description="Obfuscate only selected nodes (and others identically named)?")
	public boolean obfuscateOnlySelectedNodes;
	
	@Tunable(description="Case sensitive (when matching node names)?")
	public boolean caseSensitive;
	
	@Tunable(description="Remove all node attributes?")
	public boolean removeAllNodeAttributes;
	
	@Tunable(description="Remove all edge attributes (only edges of obfuscated nodes)?")
	public boolean removeAllEdgeAttributes;
	
	@Tunable(description="Obfuscated node name prefix:")
	public String obfuscatedNodeNamePrefix;
	
	
	public ObfuscateTask(CyApplicationManager appMgr, CyNetworkViewManager netViewMgr, CyNetworkTableManager netTableMgr) {
		this.appMgr = appMgr;
		this.netViewMgr = netViewMgr;
		this.netTableMgr = netTableMgr;
		this.random = new Random();
		this.originalToNewNameMap = new HashMap<String, String>();
		this.usedNames = new HashSet<String>();		
	}

	public void run(final TaskMonitor taskMonitor) {
		
		//create new names for all nodes / selected nodes
		for (CyNetwork network : appMgr.getSelectedNetworks()){
			for (CyNode node : network.getNodeList()){
				if (!obfuscateOnlySelectedNodes || network.getRow(node).get(CyNetwork.SELECTED, Boolean.class)){
					String originalName = network.getRow(node).get(CyNetwork.NAME, String.class).trim();
					if (!caseSensitive) originalName = originalName.toLowerCase();
					
					if (!originalToNewNameMap.containsKey(originalName)){
						String newName;
						do{
							newName = "";
							for (int i = 0; i < NAME_LENGTH; i++){
								newName += CHARACTERS.charAt(random.nextInt(CHARACTERS.length()));
							}
						}while (usedNames.contains(newName));
						
						usedNames.add(newName);
						originalToNewNameMap.put(originalName, newName);
					}
				}
			}
		}
		
		
		
		
		int networkCounter = 1;
		
		for (CyNetwork network : appMgr.getSelectedNetworks()){
			Collection<CyNetworkView> netViews = netViewMgr.getNetworkViews(network);
			CyNetworkView netView = null;
			if (!netViews.isEmpty()) netView = netViews.iterator().next();
			
			
			CyRow networkRow = network.getRow(network);
			if (obfuscateNetworkNames){
				networkRow.set(CyNetwork.NAME, "Network " + networkCounter);
				networkRow.set("shared name", "Network " + networkCounter);
				if (netView != null){
					netView.clearValueLock(BasicVisualLexicon.NETWORK_TITLE);
				}
				networkCounter++;
			}
			
			
			ArrayList<String> nodeColumnNames = new ArrayList<String>();
			for (CyColumn nodeColumn : network.getDefaultNodeTable().getColumns()){
				String columnName = nodeColumn.getName();
				if (!columnName.equals(CyNetwork.SUID) && !columnName.equals(CyNetwork.SELECTED) &&
						!columnName.equals("shared name") && !columnName.equals(CyNetwork.NAME)){
					nodeColumnNames.add(columnName);
				}
			}
			
			ArrayList<String> edgeColumnNames = new ArrayList<String>();
			for (CyColumn edgeColumn : network.getDefaultEdgeTable().getColumns()){
				String columnName = edgeColumn.getName();
				if (!columnName.equals(CyNetwork.SUID) && !columnName.equals(CyNetwork.SELECTED) &&
						!columnName.equals("shared name") && !columnName.equals(CyNetwork.NAME) &&
						!columnName.equals(CyEdge.INTERACTION) && !columnName.equals("shared interaction")){
					edgeColumnNames.add(columnName);
				}
			}
			
			
			
			//obfuscate nodes and edges in the network
			for (CyNode node : network.getNodeList()){
				CyRow nodeRow = network.getRow(node);
				String originalName = nodeRow.get(CyNetwork.NAME, String.class).trim();
				if (!caseSensitive) originalName = originalName.toLowerCase();
				
				if (originalToNewNameMap.containsKey(originalName)){
					String newName = obfuscatedNodeNamePrefix + originalToNewNameMap.get(originalName);
							
					nodeRow.set(CyNetwork.NAME, newName);
					nodeRow.set("shared name", newName);
					
					if (netView != null){
						View<CyNode> nodeView = netView.getNodeView(node);
						nodeView.clearValueLock(BasicVisualLexicon.NODE_LABEL);
					}
					
					
					
					//removing all other node attributes
					if (removeAllNodeAttributes){
						for (String nodeColumn : nodeColumnNames){
							nodeRow.set(nodeColumn, null);
						}
					}
					
					
					
					//obfuscate edges
					for (CyEdge edge : network.getAdjacentEdgeList(node, Type.ANY)){
						CyRow edgeRow = network.getRow(edge);
						
						if (removeAllEdgeAttributes){
							edgeRow.set(CyEdge.INTERACTION, "interacts with");
							edgeRow.set("shared interaction", "interacts with");
						}
						
						String sourceName = network.getRow(edge.getSource()).get(CyNetwork.NAME, String.class);
						String targetName = network.getRow(edge.getTarget()).get(CyNetwork.NAME, String.class);
						String interaction = network.getRow(edge).get(CyEdge.INTERACTION, String.class);
						String edgeName = sourceName + " (" + interaction + ") " + targetName;
						
						edgeRow.set(CyNetwork.NAME, edgeName);
						edgeRow.set("shared name", edgeName);
						
						
						if (netView != null){
							View<CyEdge> edgeView = netView.getEdgeView(edge);
							edgeView.clearValueLock(BasicVisualLexicon.EDGE_LABEL);
						}
						
						
						//removing all other edge attributes
						if (removeAllEdgeAttributes){
							for (String edgeColumn : edgeColumnNames){
								edgeRow.set(edgeColumn, null);
							}
						}
					}
				}
			}
			

			
			
			
			//delete vestigial rows (nodes/edges that only exist in tables but not in the actual network)
			ArrayList<Long> vestigialNodes = new ArrayList<Long>();
			for (CyRow nodeRow : network.getDefaultNodeTable().getAllRows()){
				long suid = nodeRow.get(CyNetwork.SUID, Long.class);
				if (network.getNode(suid) == null){
					vestigialNodes.add(suid);
				}
			}
			network.getDefaultNodeTable().deleteRows(vestigialNodes);
			
			ArrayList<Long> vestigialEdges = new ArrayList<Long>();
			for (CyRow edgeRow : network.getDefaultEdgeTable().getAllRows()){
				long suid = edgeRow.get(CyNetwork.SUID, Long.class);
				if (network.getEdge(suid) == null){
					vestigialEdges.add(suid);
				}
			}
			network.getDefaultEdgeTable().deleteRows(vestigialEdges);
			
			
			
			
			
			//clear HIDDEN_ATTRS tables
			CyTable hiddenNetworkTable = network.getTable(CyNetwork.class, CyNetwork.HIDDEN_ATTRS);
			ArrayList<Long> hiddenNetworkTableEntries = new ArrayList<Long>();
			for (CyRow row : hiddenNetworkTable.getAllRows()){
				hiddenNetworkTableEntries.add(row.get(CyNetwork.SUID, Long.class));
			}
			hiddenNetworkTable.deleteRows(hiddenNetworkTableEntries);
			
			CyTable hiddenNodeTable = network.getTable(CyNode.class, CyNetwork.HIDDEN_ATTRS);
			ArrayList<Long> hiddenNodeTableEntries = new ArrayList<Long>();
			for (CyRow row : hiddenNodeTable.getAllRows()){
				hiddenNodeTableEntries.add(row.get(CyNetwork.SUID, Long.class));
			}
			hiddenNodeTable.deleteRows(hiddenNodeTableEntries);
			
			CyTable hiddenEdgeTable = network.getTable(CyEdge.class, CyNetwork.HIDDEN_ATTRS);
			ArrayList<Long> hiddenEdgeTableEntries = new ArrayList<Long>();
			for (CyRow row : hiddenEdgeTable.getAllRows()){
				hiddenEdgeTableEntries.add(row.get(CyNetwork.SUID, Long.class));
			}
			hiddenEdgeTable.deleteRows(hiddenEdgeTableEntries);
			
			
			
			
			
			
			
			//delete custom private tables
			Map<String, CyTable> networkTables = netTableMgr.getTables(network, CyNetwork.class);
			for (String namespace : new ArrayList<String>(networkTables.keySet())){
				CyTable table = networkTables.get(namespace);
				if (!defaultTableNames.contains(namespace) && !table.isPublic()){
					netTableMgr.removeTable(network, CyNetwork.class, namespace);
				}
			}
			
			Map<String, CyTable> nodeTables = netTableMgr.getTables(network, CyNode.class);
			for (String namespace : new ArrayList<String>(nodeTables.keySet())){
				CyTable table = nodeTables.get(namespace);
				if (!defaultTableNames.contains(namespace) && !table.isPublic()){
					netTableMgr.removeTable(network, CyNode.class, namespace);
				}
			}
			
			Map<String, CyTable> edgeTables = netTableMgr.getTables(network, CyEdge.class);
			for (String namespace : new ArrayList<String>(edgeTables.keySet())){
				CyTable table = edgeTables.get(namespace);
				if (!defaultTableNames.contains(namespace) && !table.isPublic()){
					netTableMgr.removeTable(network, CyEdge.class, namespace);
				}
			}
		}
	}
}
